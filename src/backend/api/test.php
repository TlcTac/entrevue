<?php
//Version 5

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header("Content-Type: application/json; charset=utf-8");


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
mysqli_set_charset($conn, 'utf8');


$post_json = json_decode(file_get_contents('php://input'),true);

/* 
  Création de compte / Create Account
  Call by : addRegister() in register/register.page.ts
*/

$password = md5($post_json['password']); //Encrypted password with MD5
$dateNow = date('Y/m/d', $post_json['creationDate']);
$query = mysqli_query($conn,"INSERT INTO membre (idMembre,firstName,lastName,email,password,status) VALUES
    (NULL,'$post_json[firstname]','$post_json[lastname]','$post_json[email]','$password','1', '$dateNow'");
if($query) $result = json_encode(array('success'=>true, 'result'=>'success'));
else $result = json_encode(array('success'=>false, 'result'=>'error'));
