<?php
//Version 5

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header("Content-Type: application/json; charset=utf-8");


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
mysqli_set_charset($conn, 'utf8');


$post_json = json_decode(file_get_contents('php://input'),true);

/* 
  Création de compte / Create Account
  Call by : addRegister() in register/register.page.ts
*/
if($post_json['aksi']=="add-register"){
  $password = md5($post_json['password']); //Encrypted password with MD5
  $dateNow = date('Y/m/d');
  $query = mysqli_query($conn,"INSERT INTO membre (idMembre,firstName,lastName,email,password,status,creationDate) VALUES
      (NULL,'$post_json[firstname]','$post_json[lastname]','$post_json[email]','$password','1', '$dateNow')");
  if($query) {$result = json_encode(array('success'=>true));}
  else {
    $result =  json_encode(array('success'=>false,'msg'=>mysqli_error($conn)));
  }
  echo $result; 
}
/* 
  Page de Login / Login page
  Call by : prosesLogin() in login.page.ts
*/
elseif($post_json['aksi']=="login") {
  $password = md5($post_json['password']);
  $query = mysqli_query($conn, "SELECT * FROM membre WHERE email='$post_json[email]' AND password='$password'");
  $check = mysqli_num_rows($query);
  if($check>0){
    $data = mysqli_fetch_array($query);
    $datauser = array(
      'idMembre'  => $data['idMembre'],
      'email'    => $data['email'],
      'lastname'    => $data['lastName'],
      'firstname'    => $data['firstName'],
      'password' => $data['password']
    );
    if($data['status']) $result = json_encode(array('success'=>true, 'result'=>$datauser));
    else $result = json_encode(array('success'=>false, 'msg'=>'Account Inactive')); 
  } else {
    $result = json_encode(array('success'=>false, 'msg'=>'Unregister Account', 'result'=>$post_json));
  }
  echo $result;

/* 
  Mise à jour du profil / Update profile
  Call by : onClickSubmit() in mysettings/your-account/edit-profile/edit-profile.page.ts
*/
} elseif($post_json['aksi']=='update') {
  $query = mysqli_query($conn, "UPDATE membre SET 
    firstName='$post_json[firstname]',
    lastName='$post_json[lastname]' WHERE membre.idMembre='$post_json[idMembre]'");
  if($query) $result = json_encode(array('success'=>true, 'result'=>'success'));
  else $result = json_encode(array('success'=>false, 'result'=>'error'));

  echo $result;

/* 
  [ Not finished ]
  Récupere les évenements d'une ville  / Get event of a city
  Call by : ngOnInit() in search-events/where/where.page.ts
*/
} elseif($post_json['aksi']=='getEventCity'){
  $query = mysqli_query($conn, "SELECT city from event ");
  while($row = mysqli_fetch_array($query)){
    $data[] = array(
      'city' => $row['city'],
    );
  } 
  if($query) $result = json_encode($data);
  else $result = json_encode(array('success'=>false));
  echo $result;

/**
 * Lecture des evenements 
 * Call by : ngOnInit() in slides.components.ts
 */
} elseif($post_json['aksi']=='readEvent'){
  $query = mysqli_query($conn, "SELECT * FROM event");
  while($row = mysqli_fetch_array($query)){
    $data[] = array(
      'id'            => $row['id'],
      'picture'       => $row['picture'],
      'title'         => $row['title'],
      'dateEvent'     => strtotime($row['dateEvent']) ,
      'communityName' => $row['communityName'],
      'city'          => $row['city'],
      'place'         => $row['place'],
      'description'   => $row['description'],
    );
  } 
  if($query) $result = json_encode($data);
  else $result = json_encode(array('success'=>false));
  echo $result;

}