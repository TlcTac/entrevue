import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-header-back',
  templateUrl: './header-back.component.html',
  styleUrls: ['./header-back.component.scss'],
})
export class HeaderBackComponent implements OnInit {

  protected dateNow = new Date(); //Use in front
  
  constructor(private nav:NavController) { }

  ngOnInit() {}

  /**
   * Return to the previous page
   */
  onClickBack(){
    this.nav.pop();
  }
}
