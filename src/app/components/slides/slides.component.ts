import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { Event } from '../../class/event';
import { Router } from '@angular/router';
import { PostProvider } from 'src/app/services/postProvdier.service';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
})
export class SlidesComponent implements OnInit {
  @ViewChild(IonSlides) slides: IonSlides; //https://ionicframework.com/docs/api/slides/
  @Input() pastEvent:boolean; // Use for filter event : false if user from myschedule-page.ts and true from past-event.page.ts

  // Navigation buttons
  protected displayPreviousButton : string ="none";
  protected displayNextButton : string ="none";
  private timeOffset : number;
  private body : any = {'aksi':"readEvent"}
  private eventsJson : Event[];

  constructor(private nav:NavController, private eventService: PostProvider, private router:Router) { }

  ngOnInit() {
    // Get offset time
    this.timeOffset = new Date().getTimezoneOffset();
    console.log("Number of offset hours detected / Nombre d'heures de décalage détectées : "+this.timeOffset/60)

    //Get event
    this.eventService.postData(this.body,'postProvider.php').subscribe((events: Event[])=>{ 
      //Page Past Event - past-event.page.ts    
      if(this.pastEvent){
        this.eventsJson = events.filter(event => new Date(event.dateEvent).getTime() < Number(new Date().getTime().toString().substring(0,10)));
        this.eventsJson.sort(date=>new Date(date.dateEvent).getTime())
        this.eventsJson.reverse()
      } 
      // Page Event - myschedule-page.ts
      else {
        this.eventsJson = events.filter(event => new Date(event.dateEvent).getTime() > Number(new Date().getTime().toString().substring(0,10)));
        this.eventsJson.sort(date=>new Date(date.dateEvent).getTime())
      }
      console.log("Number of events to display / Nombre d'évenement à afficher "+this.eventsJson.length);
      
      //Display nav button
      if (this.eventsJson.length>1){
        this.displayPreviousButton ="none";
        this.displayNextButton ="inline-block";
      }
    })
  }

  /**
   * 
   * Old's student git : https://github.com/Meithou/AppliTeluq [dosen't work]
   */
  onClickMap(){
    console.log("Click map button");
  }

  /**
   * @todo
   * Permet de naviguer sur une vue détaillée de l'évenement / Navigate a detailed view of the event 
   * Redirection to pages/detail-pages/detail-event/detail-event.page.ts
   * @param eventCard
   */
  onClickCard(eventCard:Event){
    this.router.navigate(['tabs/detail-pages/detail-event'],{
      queryParams: {value : JSON.stringify(eventCard)},
    });
  }

  /**
   * Fonction pour naviguer entre les évenements / Function to navigate between events
   * https://ionicframework.com/docs/api/slides/
   */
  onPreviousClick(){
    this.slides.slidePrev();
    this.checkActiveIndex();
  }
  onNextClick(){
    this.slides.slideNext();
    this.checkActiveIndex();
  }

  /**
   * Fonction qui cache les boutons de navigation inutiles / Function that hides unnecessary navigation buttons
   */
  checkActiveIndex(){
    this.slides.getActiveIndex()
      .then(value => {
        if(value==this.eventsJson.length-1){
          this.displayPreviousButton = "inline-block";
          this.displayNextButton = "none";
        } else if (value==0){
          this.displayPreviousButton = "none";
          this.displayNextButton = "inline-block";
        } else {
          this.displayPreviousButton = "inline-block";
          this.displayNextButton = "inline-block";
        }
      })
      .catch(error => console.log(error));
  }
  



  protected colorTimeRemind = "black";
  /**
   * @todo En cours, j'ai un problème avec les fuseaux horaires
   * Calcul le temps restant avant l'évenement en paramètre / Calculation of the time remaining before the event in parameter
   * @param dateEvent : Date de l'évenement / Date of the event
   */
  diffDate(dateEvent: Date){
    var dateParam = new Date(dateEvent);
    var today =  new Date().getTime().toString().substring(0,10);
    var diff : any = {}                           // Initialisation du retour
    var tmp = Math.abs(dateParam.getTime()    -  Number(today) -  Math.abs(dateParam.getTimezoneOffset()*60000));
    
    tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
    diff.sec = tmp % 60;                    // Extraction du nombre de secondes
 
    tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
    diff.min = tmp % 60;                    // Extraction du nombre de minutes
 
    tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
    diff.hour = tmp % 24;                   // Extraction du nombre d'heures
     
    tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
    diff.day = tmp;
    console.log(diff)

    //Modification de la couleur en fonction du temps restant
    if(diff.day==0){
      this.colorTimeRemind = "primary";
      return "In less than "+diff.hour+ " hour";
    } else {
      return "In less than "+diff.day+" days";
    }
  }
}
