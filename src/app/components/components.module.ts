import { NgModule } from '@angular/core';

import { HeaderComponent } from './header/header.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SlidesComponent } from './slides/slides.component';
import { HeaderBackComponent } from './header-back/header-back.component';


@NgModule({
  exports: [HeaderComponent, SlidesComponent,HeaderBackComponent],
  declarations: [HeaderComponent, SlidesComponent,HeaderBackComponent],
  imports: [    
    CommonModule,
    FormsModule,
    IonicModule,
  ]
})
export class ComponentsPageModule {}


/* 
  Ce component regroupe les modules qui peuvent être importer dans les autres pages de l'application.
  This component groups the modules that can be imported into the other pages of the application.
*/