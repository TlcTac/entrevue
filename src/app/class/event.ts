export class Event {
    id: number;
    title: string;
    picture : string;
    dateEvent : Date;
    communityName : string;
    city : string;
    place: string
    description: string;
}