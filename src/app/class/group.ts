import { Event } from './event';
import { Membre } from './membre';

export class Group {
    name : string;
    place : string;
    topics : string[];
    membres : Membre[];
    events : Event[];
    picture : string;
}