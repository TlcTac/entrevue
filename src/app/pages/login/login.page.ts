import { AuthService } from './../../services/auth.service';

import { NavController, ToastController } from '@ionic/angular';
import { OnInit, Component } from '@angular/core';
import { Membre } from 'src/app/class/membre';
import { PostProvider } from 'src/app/services/postProvdier.service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
    user : Membre = {idMembre:0,firstname:"",lastname:"",email:"a",password:"a"};

  constructor(private nav: NavController,private postProvider: PostProvider,private storage: Storage,public toastCtrl: ToastController,private authService:AuthService) {}
 
  ngOnInit() {}
 
  registration(){
    this.nav.navigateForward('/login/register')
  }


  async prosesLogin(){
    if(this.user.email != "" && this.user.email != ""){let body = {email: this.user.email,password: this.user.password, aksi: 'login'};
      this.postProvider.postData(body, 'postProvider.php').subscribe(async (data: { msg: any; success: any; result: any; }) =>{
        var alertpesan = data.msg;
        if(data.success){
          this.postProvider.setActiveMembre(data.result);
          this.storage.set('session_storage', data.result);
          this.authService.signIn()
          this.nav.navigateForward('/tabs/my-schedule');
          const toast = await this.toastCtrl.create({message: 'Login Succesfully.',duration: 100});
          toast.present();
          this.user.email = "";
          this.user.password = "";
          console.log(data);
        } else {
          console.log(data.result)
          const toast = await this.toastCtrl.create({message: alertpesan,duration: 2000});
    	  toast.present();
        }
      });
    } else {
      const toast = await this.toastCtrl.create({message: 'Username or Password Invalid.',duration: 2000});
	    toast.present();
    }
  }

}