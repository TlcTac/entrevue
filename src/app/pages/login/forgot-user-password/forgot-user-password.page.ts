import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-user-password',
  templateUrl: './forgot-user-password.page.html',
  styleUrls: ['./forgot-user-password.page.scss'],
})
/**
 * @todo
 * Page pour récuperer son utilisateur / Page to retrieve your username
 */
export class ForgotUserPasswordPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
