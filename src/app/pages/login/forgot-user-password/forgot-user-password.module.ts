import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ForgotUserPasswordPage } from './forgot-user-password.page';

const routes: Routes = [
  {
    path: '',
    component: ForgotUserPasswordPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ForgotUserPasswordPage]
})
export class ForgotUserPasswordPageModule {}
