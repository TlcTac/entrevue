import { Component, OnInit } from '@angular/core';
import { Membre } from 'src/app/class/membre';
import { ToastController, NavController } from '@ionic/angular';
import { PostProvider } from 'src/app/services/postProvdier.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  private user : Membre = { idMembre:0,firstname:"Pierre",lastname:"Feuille",email:"Ciseaux",password:"" };
  private confirm_password = "";

  constructor(private toastCtrl : ToastController, private postProvider: PostProvider,private nav: NavController) { }

  ngOnInit() {
  }

  /**
   * @todo Gestion de l'activation des comptes par mail
   * Création d'un utilisateur sur le serveur
   */
  async addRegister(){
    // Toast connection : https://ionicframework.com/docs/api/toast
    if(this.user.firstname==""){
      let toast = await this.toastCtrl.create({message: 'Your first name is required',duration: 3000});
      toast.present();
    } else if (this.user.lastname==""){
      let toast = await this.toastCtrl.create({message: 'Your last name is required',duration: 3000});
      toast.present();
    } else if (this.user.email==""){
      let toast = await this.toastCtrl.create({message: 'Your email is required',duration: 3000});
      toast.present();
    } else if (this.user.password==""){
      let toast = await this.toastCtrl.create({message: 'Your password is required',duration: 2000});
      toast.present();
    } else if (this.user.password!=this.confirm_password){
      let toast = await this.toastCtrl.create({message: 'Your passwords is not the same',duration: 3000,});
      toast.present();
    } else {
      //Récupération des données entrées / Recovering input data
      let body = {firstname:this.user.firstname,lastname:this.user.lastname,email:this.user.email,password:this.user.password,aksi:"add-register"}
      console.log("Data send : ");
      console.log(body)
      //Envoi des données au serveur / Sending data to the server
      this.postProvider.postData(body,'postProvider.php').subscribe(async (data: { msg: string; success: string; }) =>{
        
        var alertSpan = data.msg
        if(data.success){
          const toast = await this.toastCtrl.create({message: 'Register succesful',duration: 3000,});
          toast.present();
          this.nav.pop();
        } else {
          const toast = await this.toastCtrl.create({message: alertSpan,duration: 3000,});
          toast.present();
        }
     
      });
    }
  }
}
