import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotUserNamePage } from './forgot-user-name.page';

describe('ForgotUserNamePage', () => {
  let component: ForgotUserNamePage;
  let fixture: ComponentFixture<ForgotUserNamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotUserNamePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotUserNamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
