import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-user-name',
  templateUrl: './forgot-user-name.page.html',
  styleUrls: ['./forgot-user-name.page.scss'],
})
/**
 * @todo 
 * Page pour récuperer son mot de passe / Page to retrieve your password
 */
export class ForgotUserNamePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
