import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ForgotUserNamePage } from './forgot-user-name.page';

const routes: Routes = [
  {
    path: '',
    component: ForgotUserNamePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ForgotUserNamePage]
})
export class ForgotUserNamePageModule {}
