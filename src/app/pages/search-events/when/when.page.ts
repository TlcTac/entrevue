import { Component, OnInit, ViewChildren, QueryList, ViewChild, ElementRef } from '@angular/core';
import { NavController } from '@ionic/angular';
import { WhenPageModule } from './when.module';
import { ChooseService } from 'src/app/services/choose.service';


@Component({
  selector: 'app-when',
  templateUrl: './when.page.html',
  styleUrls: ['./when.page.scss'],
})
export class WhenPage implements OnInit {
  
  protected dateCheck : boolean  = false;
  protected classDate : string = "";
  protected myDate = "";
  protected items : any[];

  constructor(private nav:NavController, private _chooseService:ChooseService) {}

  ngOnInit() {
    this.items = this._chooseService.getAllChooseWhen();
  }

  /**
   * La fonction est lancée lorsque le choix est changé par l'utilisateur / The function is started when the choice is changed by the user
   * @param choose voir when.page.html
   */
  onChangedRadioGroup(choose:any) : void {
    console.log("Changed" + choose.target.value)
    this._chooseService.setchooseWhenValue(choose.target.value);
  }

  onClickValidate(){
    this.nav.pop();
  }
}
