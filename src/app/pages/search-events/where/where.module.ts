import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WherePage } from './where.page';
import { ComponentsPageModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: WherePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WherePage]
})
export class WherePageModule {}
