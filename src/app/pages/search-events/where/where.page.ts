import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { PostProvider } from 'src/app/services/postProvdier.service';

@Component({
  selector: 'app-where',
  templateUrl: './where.page.html',
  styleUrls: ['./where.page.scss'],
})
export class WherePage implements OnInit {

  private eventCity : any;
  private request : any =  {aksi:"getEventCity",city:""}

  constructor(private nav:NavController,private postProvider: PostProvider) { 
  }

    /**
    * @error
    * Impossible de recupérer les data pour les afficher dans le front
    */
  ngOnInit() {
    this.eventCity = this.postProvider.postData(this.request,'postProvider.php').subscribe((data: any) =>{
      console.log(data); //Data OK
      this.eventCity = data;
    });
    console.log(this.eventCity); //Undefined ?
  }

  /**
   * Navigation
   */
  onClickBack(){
    this.nav.pop();
  }
}
