import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatPage } from './what.page';

describe('WhatPage', () => {
  let component: WhatPage;
  let fixture: ComponentFixture<WhatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
