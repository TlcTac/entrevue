import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ChooseService } from 'src/app/services/choose.service';

@Component({
  selector: 'app-what',
  templateUrl: './what.page.html',
  styleUrls: ['./what.page.scss'],
})
export class WhatPage implements OnInit {

  protected items : any[];
  constructor(private _chooseService:ChooseService, private nav:NavController) {}

  ngOnInit() {
    this.items = this._chooseService.getAllChooseWhat();
  }

  onChangedRadioGroup(choose:any) : void {
    console.log("Changed" + choose.target.value)
    this._chooseService.setchooseWhatValue(choose.target.value);
  }

  onClickValidate(){
    this.nav.pop();
  }
}
