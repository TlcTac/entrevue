import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ChooseService } from 'src/app/services/choose.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-events',
  templateUrl: './search-events.page.html',
  styleUrls: ['./search-events.page.scss'],
})
export class SearchEventsPage implements OnInit {
  protected chooseWhenValueSubscribe : Subscription;
  protected chooseWhatValueSubscribe : Subscription;

  private chooseWhenValue : string = "Anytime";
  private chooseWhatValue : string = "Anytime";
  private chooseWhereValue : string = "Montréal";
  private choose : any;
  
  constructor(private nav:NavController, private _chooseService:ChooseService,private router:Router) {}

  ngOnInit() {
    this.chooseWhenValueSubscribe = this._chooseService.getchooseWhenValue().subscribe(
      params=>{this.chooseWhenValue=params}
    );
    this.chooseWhatValueSubscribe = this._chooseService.getchooseWhatValue().subscribe(
      params=>{this.chooseWhatValue=params}
    );   
  }

  /**
   * Navigation
   */
  onClickWhen(){
    this.nav.navigateForward('tabs/search-events/when');
  }
  onClickWhere(){
    this.nav.navigateForward('tabs/search-events/where');
  }
  onClickWhat(){
    this.nav.navigateForward('tabs/search-events/what');
  }
  /**
   * @todo Script PHP permettant de faire un recherche directement dans la database / PHP script allowing to search directly in the database
   * Redirige sur une page de recherche avec les data qui sont passées par URL / Redirect to a search page with data that is passed by URL.
   */
  onClickArrow(){ 
    this.choose = {chooseWhenValue : this.chooseWhenValue,chooseWhatValue:this.chooseWhatValue,chooseWhereValue:this.chooseWhereValue,aksi:"researchEvent"}
    this.router.navigate(['tabs/search-events/search'],{
      queryParams: {value : JSON.stringify(this.choose)},
    });
  }
}
