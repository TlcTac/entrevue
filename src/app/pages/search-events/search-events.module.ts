import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchEventsPage } from './search-events.page';
import { ComponentsPageModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: SearchEventsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SearchEventsPage]
})
export class SearchEventsPageModule {}
