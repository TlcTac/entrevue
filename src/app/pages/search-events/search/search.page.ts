import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { PostProvider } from 'src/app/services/postProvdier.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  private data : any[];

  constructor(private nav: NavController, private postProvider:PostProvider,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    /**
     * @error 
     * Même problème que where.page.ts 
     */
    this.activatedRoute.queryParams.subscribe((param)=>{
      this.data = JSON.parse(param.value);
      console.log(this.data)
      this.postProvider.postData(this.data,'postProvider.php').subscribe( async (data: any) =>{
        console.log(data);
      });
    });
  }

  /**
   * Up and Down button
   */
  onClickScrollToBottom(){
    this.content.scrollToBottom(1500);
  }
  onClickScrollToTop(){
    this.content.scrollToTop(1500);
  }
  onClickView(){
    this.nav.navigateForward("tabs/detail-pages/detail-event");
  }
}
