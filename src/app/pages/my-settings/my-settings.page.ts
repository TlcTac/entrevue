import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-my-settings',
  templateUrl: './my-settings.page.html',
  styleUrls: ['./my-settings.page.scss'],
})
export class MySettingsPage implements OnInit {

  constructor(private auth:AuthService, private nav:NavController) { }

  ngOnInit() {
  }

  /**
   * Navigation
   */
  onClickYourAccount(){
    this.nav.navigateForward("tabs/my-settings/your-account");
  }
  onClickAppsSettings(){
    this.nav.navigateForward("tabs/my-settings/apps-settings");
  }
  /**
   * Come back to login page
   */
  onClickSignOut(){
    this.auth.signOut();
  }
}
