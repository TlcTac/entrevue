import { Component, OnInit } from '@angular/core';

import { NavController } from '@ionic/angular';
import { PostProvider } from 'src/app/services/postProvdier.service';

@Component({
  selector: 'app-your-account',
  templateUrl: './your-account.page.html',
  styleUrls: ['./your-account.page.scss'],
})
export class YourAccountPage implements OnInit {
  public activeMembre : any;

  constructor(private nav:NavController, private postProvider:PostProvider) { 
  }

  ngOnInit() {
    this.activeMembre = this.postProvider.getActiveMembre();
    console.log(this.activeMembre)
  }

  onClickEditProfile(){
    this.nav.navigateForward('tabs/my-settings/your-account/edit-profile');
  }

}
