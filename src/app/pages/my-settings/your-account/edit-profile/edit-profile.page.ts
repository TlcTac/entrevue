import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { PostProvider } from 'src/app/services/postProvdier.service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  private activeMembre : any;
  protected bufferActiveMembre : PostProvider;
  constructor(private nav:NavController, private postProvider:PostProvider, private storage: Storage,private toastCtrl: ToastController) { }

  ngOnInit() {
    this.activeMembre = this.postProvider.getActiveMembre();
  }

/**
 * Update profile in serve
 */
  onClickSubmit(){
    this.activeMembre.aksi ='update';
    console.log("Requete de modification de profile envoyée / Profile change request sent" );
    console.log(this.activeMembre);
    this.postProvider.postData(this.activeMembre, 'postProvider.php').subscribe(async (data: { msg: any; success: any; }) =>{
      var alertpesan = data.msg;
      if(data.success){
        console.log("Requete de modification réussite, modification de la variable locale / Success modification request, modification of the local variable");
        this.postProvider.setActiveMembre(this.activeMembre);
        this.nav.pop();
        const toast = await this.toastCtrl.create({message: 'Update Succesfully.',duration: 100});
        toast.present();
      } else {
        const toast = await this.toastCtrl.create({message: alertpesan,duration: 2000});
        toast.present();
      }
    });
  }
}
