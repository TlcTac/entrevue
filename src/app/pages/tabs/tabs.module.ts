import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs.page';
import { AuthGuard } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children : [
      { path: 'my-schedule', children:[{path:'', loadChildren: '../my-schedule/my-schedule.module#MySchedulePageModule',canActivate: [AuthGuard],data : {role : 'USER'}}]},
      { path: 'my-settings', children:[{path:'', loadChildren: '../my-settings/my-settings.module#MySettingsPageModule',canActivate: [AuthGuard],data : {role : 'USER'} }]},
      { path: 'search-events', children:[{path:'', loadChildren: '../search-events/search-events.module#SearchEventsPageModule',canActivate: [AuthGuard],data : {role : 'USER'} }]},
      { path: 'my-groups', children:[{path:'', loadChildren: '../my-groups/my-groups.module#MyGroupsPageModule',canActivate: [AuthGuard],data : {role : 'USER'} }]}, ],
     canActivate: [AuthGuard],
     data : {
       role : 'USER'
     },
    
  },];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
