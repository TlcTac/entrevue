import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailEventPage } from './detail-event.page';
import { ComponentsPageModule } from 'src/app/components/components.module';
import { TabsPageModule } from '../../tabs/tabs.module';

const routes: Routes = [
  {
    path: '',
    component: DetailEventPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailEventPage]
})
export class DetailEventPageModule {}
