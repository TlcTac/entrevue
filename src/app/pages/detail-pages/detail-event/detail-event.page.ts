import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-event',
  templateUrl: './detail-event.page.html',
  styleUrls: ['./detail-event.page.scss'],
})
export class DetailEventPage implements OnInit {
  public data : any[];


  constructor(private activatedRoute:ActivatedRoute) {}

  /**
   * @todo 
   */
  ngOnInit() {
    //Get URL parmaters from onClickCard() in components/slides/slides.compoenent.ts
    this.activatedRoute.queryParams.subscribe((param)=>{
      this.data = JSON.parse(param.value);
    });
  }
}
