import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailTopicPage } from './detail-topic.page';
import { ComponentsPageModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: DetailTopicPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailTopicPage]
})
export class DetailTopicPageModule {}
