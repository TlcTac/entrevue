import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { GroupService } from 'src/app/services/group.service';
import { Group } from 'src/app/class/group';

@Component({
  selector: 'app-my-groups',
  templateUrl: './my-groups.page.html',
  styleUrls: ['./my-groups.page.scss'],
})
export class MyGroupsPage implements OnInit {

  protected groups: Group[];
  constructor(private nav: NavController, private _groupService: GroupService) {}

  ngOnInit() {
    this.groups = this._groupService.getAllGroups();
  }

  /**
   * Navigation
   */
  onClickAddGroups(){
    this.nav.navigateForward("tabs/my-groups/add-groups");
  }
  onClickGroupCard(){
    this.nav.navigateForward("tabs/detail-pages/detail-group")
  }
}
