import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGroupsPage } from './add-groups.page';

describe('AddGroupsPage', () => {
  let component: AddGroupsPage;
  let fixture: ComponentFixture<AddGroupsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGroupsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGroupsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
