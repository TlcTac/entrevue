import { Component, OnInit, ViewChild } from '@angular/core';
import { GroupService } from 'src/app/services/group.service';
import { Group } from 'src/app/class/group';
import { NavController, IonContent } from '@ionic/angular';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  protected groups : Group[];
  constructor(private _groupService:GroupService, private nav:NavController) { }

  ngOnInit() {
    this.groups = this._groupService.getAllGroups()
  }
  onClickView(){
    this.nav.navigateForward("tabs/detail-pages/detail-group");
  }

    /**
   * Up and Down button
   */
  onClickScrollToBottom(){
    this.content.scrollToBottom(1500);
  }
  onClickScrollToTop(){
    this.content.scrollToTop(1500);
  }
}
