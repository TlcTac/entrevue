import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TopicService } from 'src/app/services/topic.service';
import { Topic } from 'src/app/class/topic';

@Component({
  selector: 'app-add-groups',
  templateUrl: './add-groups.page.html',
  styleUrls: ['./add-groups.page.scss'],
})
export class AddGroupsPage implements OnInit {

  protected topics : Topic[];
  constructor(private nav:NavController, private _topicService:TopicService) { }

  ngOnInit() {
    this.topics = this._topicService.getAllTopics();
  }

  /**
   * Navigation
   */
  onClickWhere(){
    this.nav.navigateForward("/tabs/my-groups/add-groups/where");
  }
  onClickArrow(){
    this.nav.navigateForward("/tabs/my-groups/add-groups/search");
  }
  onClickTopicCard(){
    this.nav.navigateForward("tabs/detail-pages/detail-topic");
  }
}
