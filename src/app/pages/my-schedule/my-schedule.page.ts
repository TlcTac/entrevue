import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-my-schedule',
  templateUrl: './my-schedule.page.html',
  styleUrls: ['./my-schedule.page.scss'],
})
export class MySchedulePage implements OnInit {
  public pastEvent = false;
  
  constructor(private nav: NavController) { }

  ngOnInit() {
  }

  onClickItinary(){
    this.nav.navigateForward('tabs/my-schedule/past-event');
  }
}
