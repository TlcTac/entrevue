import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastEventPage } from './past-event.page';

describe('PastEventPage', () => {
  let component: PastEventPage;
  let fixture: ComponentFixture<PastEventPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastEventPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastEventPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
