import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-past-event',
  templateUrl: './past-event.page.html',
  styleUrls: ['./past-event.page.scss','../../../components/header/header.component.scss'],
})
export class PastEventPage implements OnInit {
  public pastEvent = true;
  constructor() { }

  ngOnInit() {
  }


}
