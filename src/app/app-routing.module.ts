import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule'},
  { path: 'tabs/my-schedule/past-event', loadChildren: './pages/my-schedule/past-event/past-event.module#PastEventPageModule' },
  { path: 'tabs/search-events/when', loadChildren: './pages/search-events/when/when.module#WhenPageModule' },
  { path: 'tabs/search-events/where', loadChildren: './pages/search-events/where/where.module#WherePageModule' },
  { path: 'tabs/search-events/what', loadChildren: './pages/search-events/what/what.module#WhatPageModule' },
  { path: 'tabs/search-events/search', loadChildren: './pages/search-events/search/search.module#SearchPageModule' },
  { path: 'tabs/my-settings/your-account', loadChildren: './pages/my-settings/your-account/your-account.module#YourAccountPageModule' },
  { path: 'tabs/my-settings/your-account/edit-profile', loadChildren: './pages/my-settings/your-account/edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'tabs/my-settings/apps-settings', loadChildren: './pages/my-settings/apps-settings/apps-settings.module#AppsSettingsPageModule' },
  { path: 'tabs/my-groups/add-groups', loadChildren: './pages/my-groups/add-groups/add-groups.module#AddGroupsPageModule' },
  { path: 'tabs/my-groups/add-groups/where', loadChildren: './pages/my-groups/add-groups/where/where.module#WherePageModule' },
  { path: 'tabs/my-groups/add-groups/search', loadChildren: './pages/my-groups/add-groups/search/search.module#SearchPageModule' },
  { path: 'tabs/detail-pages/detail-event', loadChildren: './pages/detail-pages/detail-event/detail-event.module#DetailEventPageModule' },
  { path: 'tabs/detail-pages/detail-group', loadChildren: './pages/detail-pages/detail-group/detail-group.module#DetailGroupPageModule' },
  { path: 'tabs/detail-pages/detail-topic', loadChildren: './pages/detail-pages/detail-topic/detail-topic.module#DetailTopicPageModule' },
  { path: 'login/register', loadChildren: './pages/login/register/register.module#RegisterPageModule' },
  { path: 'login/forgot-user-name', loadChildren: './pages/login/forgot-user-name/forgot-user-name.module#ForgotUserNamePageModule' },
  { path: 'login/forgot-user-password', loadChildren: './pages/login/forgot-user-password/forgot-user-password.module#ForgotUserPasswordPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
