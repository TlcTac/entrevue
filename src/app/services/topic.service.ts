import { Injectable } from '@angular/core';
import { Topic } from '../class/topic';

@Injectable({
  providedIn: 'root'
})
/**
 * Call by my-groups/add-groups/add-groups.page.ts
 */
export class TopicService {

  private topics : Topic[];
  constructor() { 
    this.topics = [
      {picture:"assets/img/mcgill.jpg",name:"sport"},
      {picture:"assets/img/mcgill.jpg",name:"cooking"},
      {picture:"assets/img/mcgill.jpg",name:"sport2"},
      {picture:"assets/img/mcgill.jpg",name:"cooking2"},
      {picture:"assets/img/mcgill.jpg",name:"sport3"},
      {picture:"assets/img/mcgill.jpg",name:"cooking3"},
      {picture:"assets/img/mcgill.jpg",name:"sport4"},
      {picture:"assets/img/mcgill.jpg",name:"cooking4"},
      {picture:"assets/img/mcgill.jpg",name:"sport5"},
      {picture:"assets/img/mcgill.jpg",name:"cooking5"},
    ];
  }
  getAllTopics(){
    return this.topics;
  }
}
