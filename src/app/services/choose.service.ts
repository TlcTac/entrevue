import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

/**
 * Service call by search-events component
 */
export class ChooseService {
    private chooseWhen : any[];
    private chooseWhenValue = new Subject<any>();
    private chooseWhat : any[];
    private chooseWhatValue = new Subject<any>();

    constructor() {
      this.chooseWhen = [
        {name:"Anytime", value:"anytime"},
        {name:"Today", value:"today"},
        {name:"Tommorrow", value:"tommorrow"},
        {name:"This weekend", value:"this-weekend"},
        {name:"Next Month", value:"next-month"},
      ]
      this.chooseWhat = [
        {name:"Anything", value:"anything"},
        {name:"Sport", value:"sport"},
        {name:"Cooking", value:"cooking"},
        {name:"Reading", value:"reading"},
        {name:"Walking", value:"walking"},
        {name:"Running", value:"running"}
        ]
    }

    setchooseWhenValue(value:string){
        this.chooseWhenValue.next(value);
    }
    getchooseWhenValue() : Observable<any>{
        return this.chooseWhenValue;
    }
    getAllChooseWhen(){
        return this.chooseWhen;
    }
    setchooseWhatValue(value:string){
      this.chooseWhatValue.next(value);
    }
    getchooseWhatValue() : Observable<any>{
        return this.chooseWhatValue;
    }
    getAllChooseWhat(){
        return this.chooseWhat;
    }
}
