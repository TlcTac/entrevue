import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostProvider {
  PHP_API_SERVER = "http://social.licef.ca/api/";
  //PHP_API_SERVER = "http://206.167.88.82/api/";
  //PHP_API_SERVER = "http://127.0.0.1/api/";
  postProvider : any;
  public activeMembre : any;


  constructor(private httpClient: HttpClient) { 
  }

  postData(data:any, postType:string) {
    const httpOptions = {headers: new HttpHeaders({ })}
    httpOptions.headers.append('Content-Type', 'application/json');
    httpOptions.headers.append('Access-Control-Allow-Origin','*');
    this.postProvider = this.httpClient.post(this.PHP_API_SERVER+postType, JSON.stringify(data), httpOptions)
    .pipe(map((response: any) => this.postProvider=response));
    return this.postProvider;
  }

  getActiveMembre(){
    this.activeMembre.role="user";
    return this.activeMembre;
  }
  setActiveMembre(activeMembre:any){
    console.log("[Service PostProvider] Update Membre")
    console.log(activeMembre)
    this.activeMembre = activeMembre;
  }
}
