import { Injectable } from '@angular/core';
import { Group } from '../class/group';

@Injectable({
  providedIn: 'root'
})
/**
 * @todo
 */
export class GroupService {
    private groups : Group[];

  constructor() { }

  getAllGroups(){
      return this.groups;
  }
}
