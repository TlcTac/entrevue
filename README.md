Créer par M. Galliot - mael.galliot@gmail.com 

# Entrevue

Application mobile ayant pour but de recommander des évenements culturelles dans Montréal. 
L'objectif de cette application est d'aider les personnes sans activiés à garder un lien sociale.

# 1. Création et mise en place du projet 

# 1.1 Mise en place de l'environnement Ionic avec Angular et Cordova

[Ionic](https://ionicframework.com/) est un framework permettant de mettre en place l'intégralité de l'envrionnement. 
Ionic utilise par défaut [Angular](https://angular.io/) et se sert de [Cordova](https://cordova.apache.org/) pour compiler l'applciation sur Android et IOS
Pour mettre en place l'environnement de développement de zéro voici les étapes à suivre :

0. (Optionnel) Si vous êtes sûr Windows, il n'y a pas git nativement, il faut l'installer : [git](https://git-scm.com/download/win)
    - Choisir de "Use Windows' default console window"
    - Lancer le cmd windows
1. Installer [Node.js](https://nodejs.org/en/)
2. Installer Ionic : `sudo npm install -g ionic`
3. Cloner le répertoire git : `git clone https://gitlab.com/TlcTac/entrevue.git`
4. Aller dans le repertoire : `cd entrevue`
5. (Optionnel) Rajouter [Angular Material](https://material.angular.io/) : `ng add @angular/material`
6. Lancer l'environnement Ionic Lab : `ionic serve -l` (dire Y pour Ionic Lab)


A partir de la l'environnement est mise en place, il suffit de lancer `ionic serve -l` et de développer
PS : Pour créer un projet : `ionic start blank --type=angular` -- [Pour plus d'info](https://ionicframework.com/docs/cli/commands/start)

# 1.2 Tester sur les équipements réels
Grâce à Cordova, on peut directement tester par l'USB l'application.

Android :

1. `ionic cordova platform rm android`
2. `ionic cordova platform add android`
3. `ionic cordova run android`

IOS :

1. `ionic cordova platform rm ios`
2. `ionic cordova platform add ios`
3. `ionic cordova run ios`


# Bilbiographie 
[Partie 1.1 et 1.2](https://www.djamware.com/post/5be52ce280aca72b942e31bc/ionic-4-angular-7-and-cordova-tutorial-build-crud-mobile-apps)


## Mise en place de l'éditeur : Visual Studio Code 

Pour développer, je vous recommande d'utiliser Visual Code Studio, qui permet d'avoir un console windows intégrée : 
1. Installer [Visual Studio Code](https://code.visualstudio.com/)
2. Lancer le terminal Terminal>New ou Ctrl+Shift+ù